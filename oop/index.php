<?php

// Import class Animal
require_once 'animal.php';

// Import class Frog
require_once 'Frog.php';

// Import class Ape
require_once 'Ape.php';

// Instance untuk class Animal
$sheep = new Animal("shaun");

echo $sheep->name . "\n";         // "shaun"
echo $sheep->legs . "\n";         // 4
echo $sheep->cold_blooded . "\n"; // "no"

// Instance untuk class Ape
$sungokong = new Ape("kera sakti");
echo $sungokong->name . "\n";     // "kera sakti"
echo $sungokong->yell() . "\n";   // "Auooo"

// Instance untuk class Frog
$kodok = new Frog("buduk");
echo $kodok->name . "\n";         // "buduk"
echo $kodok->jump() . "\n";       // "hop hop"
