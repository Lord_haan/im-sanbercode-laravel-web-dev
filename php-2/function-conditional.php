<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function</title>
</head>

<body>
    <h1>Berlatih Function PHP</h1>

    <?php

    echo "<h3> Soal No 1 Greetings </h3>";

    function greetings($name)
    {
        echo "Halo " . ucfirst($name) . ", Selamat Datang di Sanbercode!";
    }

    greetings("Bagas");
    greetings("Wahyu");
    greetings("nama peserta");

    echo "<br>";

    echo "<h3>Soal No 2 Reverse String</h3>";

    function reverseString($string)
    {
        $length = strlen($string);
        $result = "";

        for ($i = $length - 1; $i >= 0; $i--) {
            $result .= $string[$i];
        }

        echo $result;
    }

    reverseString("nama peserta");
    reverseString("Sanbercode");
    reverseString("We Are Sanbers Developers");

    echo "<br>";

    echo "<h3>Soal No 3 Palindrome </h3>";

    function isPalindrome($string)
    {
        $length = strlen($string);
        $isPalindrome = true;

        for ($i = 0; $i < $length / 2; $i++) {
            if ($string[$i] != $string[$length - 1 - $i]) {
                $isPalindrome = false;
                break;
            }
        }

        if ($isPalindrome) {
            echo "true";
        } else {
            echo "false";
        }
    }

    isPalindrome("civic");   // true
    isPalindrome("nababan"); // true
    isPalindrome("jambaban"); // false
    isPalindrome("racecar");  // true

    echo "<h3>Soal No 4 Tentukan Nilai </h3>";

    function tentukanNilai($nilai)
    {
        if ($nilai >= 85 && $nilai <= 100) {
            echo "Sangat Baik";
        } elseif ($nilai >= 70 && $nilai < 85) {
            echo "Baik";
        } elseif ($nilai >= 60 && $nilai < 70) {
            echo "Cukup";
        } else {
            echo "Kurang";
        }
    }

    tentukanNilai(98); // Sangat Baik
    tentukanNilai(76); // Baik
    tentukanNilai(67); // Cukup
    tentukanNilai(43); // Kurang

    ?>

</body>

</html>
